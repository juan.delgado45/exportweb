/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.exportweb.persistence;

import co.edu.sena.exportweb.model.ApprenticeCount;
import co.edu.sena.exportweb.model.LateArrival;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class LateArrivalDAO implements ILateArrivalDAO {

    @PersistenceContext
    private EntityManager entityManager;
    
    @Override
    public LateArrival findById(Integer id) throws Exception {
        try {
            return entityManager.find(LateArrival.class, id);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public List<LateArrival> findAll() throws Exception {
        try {
            Query query = entityManager.createNamedQuery("LateArrival.findAll");
            return query.getResultList();
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public List<LateArrival> findByDateRange(Date date1, Date date2) throws Exception {
        try {
            Query query = entityManager.createQuery("SELECT l FROM LateArrival l WHERE l.dateArrival BETWEEN :date1 AND :date2")
                                                    .setParameter("date1", date1)
                                                    .setParameter("date2", date2);
            return query.getResultList();
        } catch (RuntimeException e) {
            throw e;
        }
    }
    
    @Override
    public List<ApprenticeCount> findGroupByApprentice() throws Exception {
        try {
            Query query = entityManager.createQuery(
                    "SELECT NEW "+
                        "co.edu.sena.exportweb.model.ApprenticeCount(l.documentApprentice.document, COUNT(l.id)) "+
                        "FROM LateArrival l GROUP BY l.documentApprentice.document");
            return (List<ApprenticeCount>) query.getResultList();
        } catch (RuntimeException e) {
            throw e;
        }
    }
    
}
