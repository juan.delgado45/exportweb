/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.exportweb.persistence;


import co.edu.sena.exportweb.model.Course;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class CourseDAO implements ICourseDAO {

    @PersistenceContext
    private EntityManager entityManager;
    
    @Override
    public Course findById(Integer id) throws Exception {
        try {
            return entityManager.find(Course.class, id);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public List<Course> findAll() throws Exception {
        try {
            Query query = entityManager.createNamedQuery("Course.findAll");
            return query.getResultList();
        } catch (RuntimeException e) {
            throw e;
        }
    }
    
}
