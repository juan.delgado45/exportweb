/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.exportweb.persistence;

import co.edu.sena.exportweb.model.Apprentice;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class ApprenticeDAO implements IApprenticeDAO {

    @PersistenceContext
    private EntityManager entityManager;
    
    @Override
    public Apprentice findById(Long document) throws Exception {
        try {
            return entityManager.find(Apprentice.class, document);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public List<Apprentice> findAll() throws Exception {
        try {
            Query query = entityManager.createNamedQuery("Apprentice.findAll");
            return query.getResultList();
        } catch (RuntimeException e) {
            throw e;
        }
    }
    
}
