/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/SessionLocal.java to edit this template
 */
package co.edu.sena.exportweb.business;

import co.edu.sena.exportweb.model.Apprentice;
import java.io.OutputStream;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface ApprenticeBeanLocal {
    public Apprentice findById(Long document) throws Exception;
    public List<Apprentice> findAll() throws Exception;
    public void exportPDFApprentices(OutputStream outputStream) throws Exception;
    public void sendEmail(String to, String subject) throws Exception;
    public void sendEmailAttachment(String to, String subject, long document) throws Exception;
}
