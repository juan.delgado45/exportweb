/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package co.edu.sena.exportweb.view;

import co.edu.sena.exportweb.business.LateArrivalBeanLocal;
import co.edu.sena.exportweb.utils.MessageUtils;
import java.io.IOException;
import java.io.OutputStream;
import javax.ejb.EJB;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.commons.io.output.CountingOutputStream;

/**
 *
 * @author Aprendiz
 */
public class LateArrivalPDFView {
    
    @EJB
    private LateArrivalBeanLocal lateArrivalBean;
    /**
     * Creates a new instance of LateArrivalPDFView
     */
    public LateArrivalPDFView() {
    }
    public void exportPDF(){
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();
        ec.responseReset();
        ec.setResponseHeader("Content-Type", "application/pdf");
        ec.setResponseHeader("Content-Disposition", "attachment;filename=\"late_arrival_count.pdf\"");
        
        try(OutputStream output = ec.getResponseOutputStream()) {
            lateArrivalBean.exportPDFLateArrivals(output);
            //obtener el tamaño del archivo
            try(CountingOutputStream co = new CountingOutputStream(output)) {
                ec.setResponseContentLength((int) co.getByteCount());
                co.close();
            } catch (IOException e) {
                MessageUtils.addErrorMessage("Error al obtener el tamaño del archivo "+ e.getMessage());
            }
        } catch (Exception e) {
            MessageUtils.addErrorMessage("Error al intentar crear el PDF "+ e.getMessage());
        }
        fc.responseComplete();
    }
}
