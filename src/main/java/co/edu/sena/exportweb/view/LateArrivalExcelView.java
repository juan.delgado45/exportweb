/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package co.edu.sena.exportweb.view;

import co.edu.sena.exportweb.business.LateArrivalBeanLocal;
import co.edu.sena.exportweb.utils.MessageUtils;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import javax.ejb.EJB;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.commons.io.output.CountingOutputStream;

/**
 *
 * @author Aprendiz
 */
public class LateArrivalExcelView {
    private Date date1;
    private Date date2;
    
    @EJB
    private LateArrivalBeanLocal lateArrivalBean;
    /**
     * Creates a new instance of LateArrivalExcelView
     */
    public LateArrivalExcelView() {
    }

    public Date getDate1() {
        return date1;
    }

    public void setDate1(Date date1) {
        this.date1 = date1;
    }

    public Date getDate2() {
        return date2;
    }

    public void setDate2(Date date2) {
        this.date2 = date2;
    }
    
    public void exportExcel(){
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();
        ec.responseReset();
        ec.setResponseHeader("Content-Type", "application/vns.ms-excel");
        ec.setResponseHeader("Content-Disposition", "attachment;filename=\"late_arrival_range.xlsx\"");
        
        try(OutputStream output = ec.getResponseOutputStream()) {
            lateArrivalBean.exporExcel(output,date1,date2);
            //obtener el tamaño del archivo
            try(CountingOutputStream co = new CountingOutputStream(output)) {
                ec.setResponseContentLength((int) co.getByteCount());
                co.close();
            } catch (IOException e) {
                MessageUtils.addErrorMessage("Error al obtener el tamaño del archivo "+ e.getMessage());
            }
        } catch (Exception e) {
            MessageUtils.addErrorMessage("Error al intentar crear el excel "+ e.getMessage());
        }
        fc.responseComplete();
    
    }
}
