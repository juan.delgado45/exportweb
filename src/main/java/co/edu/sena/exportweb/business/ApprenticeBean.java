/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package co.edu.sena.exportweb.business;

import co.edu.sena.exportweb.model.Apprentice;
import co.edu.sena.exportweb.model.Course;
import co.edu.sena.exportweb.model.LateArrival;
import co.edu.sena.exportweb.persistence.IApprenticeDAO;
import co.edu.sena.exportweb.utils.Constants;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.pdfa.PdfADocument;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class ApprenticeBean implements ApprenticeBeanLocal {
     private File temporalfile; 
    @EJB
    private IApprenticeDAO apprenticeDAO;

    @Override
    public Apprentice findById(Long document) throws Exception {
        if (document == 0) {
            throw new Exception("El documento es obligatorio");
        }

        return apprenticeDAO.findById(document);
    }

    @Override
    public List<Apprentice> findAll() throws Exception {
        return apprenticeDAO.findAll();
    }

    @Override
    public void exportPDFApprentices(OutputStream outputStream) throws Exception {
        PdfWriter pdfWriter = new PdfWriter(outputStream);
        PdfDocument pdfDocument = new PdfDocument(pdfWriter);
        Document document = new Document(pdfDocument);

        Paragraph paragraph = new Paragraph("Reporte de aprendices");
        paragraph.setFontSize(14);
        paragraph.setTextAlignment(TextAlignment.CENTER);
        paragraph.setBold();
        paragraph.setFontColor(ColorConstants.ORANGE);
        document.add(paragraph);
        document.add(new Paragraph(""));//salto de linea

        float[] columsWidths = {150f, 150f, 150f};
        Table table = new Table(columsWidths);
        table.addCell(new Cell().add(new Paragraph("Documento")));
        table.addCell(new Cell().add(new Paragraph("Nombre")));
        table.addCell(new Cell().add(new Paragraph("Programa")));
        
        List<Apprentice> apprentices= findAll();
        
        for (Apprentice apprentice : apprentices) {
            table.addCell(new Cell().add(new Paragraph(String.valueOf(apprentice.getDocument()))));
            table.addCell(new Cell().add(new Paragraph(apprentice.getFullName())));
            table.addCell(new Cell().add(new Paragraph(apprentice.getIdCourse().getCareer())));
            
        }
        document.add(table);
        document.close();
        pdfWriter.close();
        outputStream.close();
        System.out.println("PDF creado");
    }

    @Override
    public void sendEmail(String to, String subject) throws Exception {
        Properties props = new Properties();
        props.put("mail.transport.protocol", Constants.PROTOCOL);
        props.put("mail.smtp.host", Constants.HOST);
        props.put("mail.smtp.socketFactory.port", Constants.SOCKET_FACTORY_PORT);
        props.put("mail.smtp.socketFactory.class", Constants.SOCKET_FACTORY);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", Constants.PORT);
        props.put("mail.smtp.user", Constants.USER);
        props.put("mail.smtp.password", Constants.PASSWORD);
        props.put("mail.smtp.starttls.enable", "true");

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        String bodyMessage = setMessage();
        InternetAddress from = new InternetAddress(props.getProperty("mail.smtp.user"));
        message.setFrom(from);
        message.setSubject(subject);
        message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(to));//destinatario
        message.setContent(bodyMessage, "text/html; charset=utf-8");//cuerpo del mensaje

        Transport transport = session.getTransport(Constants.PROTOCOL);
        transport.connect(props.getProperty("mail.smtp.host"), props.getProperty("mail.smtp.user"),
                props.getProperty("mail.smtp.password"));
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();

    }

    public String setMessage() throws Exception {
        String message = "";
        try {
            List<Apprentice> apprentices = findAll();
            message = "<p>cordial saludo se envia e reporte de aprendices registrados en ARLET</p>"
                    + "<table border ='1'>"
                    + "<tr><th>Documento</th><tr><th>Nombre</th><tr><th>Programa</th></tr>";
            for (Apprentice apprentice : apprentices) {
                message += "<tr>"
                        + "<td>" + apprentice.getDocument() + "</td>"
                        + "<td>" + apprentice.getFullName() + "</td>"
                        + "<td>" + apprentice.getIdCourse().getCareer() + "</td>"
                        + "</tr>";
            }
            message += "</table>";
        } catch (Exception e) {
            throw e;
        }
        return message;
    }

    public void exporExcelTemp(long document) throws Exception {
        Apprentice apprentice = findById(document);
        List<LateArrival> lateArrivalList = (List<LateArrival>) apprentice.getLateArrivalCollection();
        
        temporalfile= File.createTempFile("latearrivals", ".xlsx");
                
        Workbook workbook= new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Llegadas tarde");
        //estilo para encabezados
        CellStyle styleHeader = workbook.createCellStyle();
        styleHeader.setFillForegroundColor(IndexedColors.DARK_GREEN.getIndex());//color letra
        styleHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleHeader.setBorderBottom(BorderStyle.THIN);
        styleHeader.setBorderBottom(BorderStyle.THIN);
        styleHeader.setBorderBottom(BorderStyle.THIN);
        styleHeader.setBorderBottom(BorderStyle.THIN);
        
        String[] titles = {"id","Fecha","Observaciones","Documento aprendiz", "Nombre Aprendiz"};
        Row row = sheet.createRow(0);
        //crear el encabezado
        for (int i = 0; i < titles.length; i++) {
            org.apache.poi.ss.usermodel.Cell cell = row.createCell(i);
            cell.setCellStyle(styleHeader);
            cell.setCellValue(titles[i]);
        }
        
        CreationHelper createnHelper = workbook.getCreationHelper();
        CellStyle dataCellStyle = workbook.createCellStyle();
        dataCellStyle.setDataFormat(createnHelper.createDataFormat().getFormat("yyyy-MM-dd"));
        //colocamos los datos en las siguientes filas
       for (int i = 0; i < lateArrivalList.size(); i++) {
            row = sheet.createRow(i + 1);
            row.createCell(0).setCellValue(lateArrivalList.get(i).getId());
            org.apache.poi.ss.usermodel.Cell dateArraivalcell = row.createCell(1);
            dateArraivalcell.setCellStyle(dataCellStyle);
            dateArraivalcell.setCellValue(lateArrivalList.get(i).getDateArrival());
            row.createCell(2).setCellValue(lateArrivalList.get(i).getObservations());
            row.createCell(3).setCellValue(lateArrivalList.get(i).getDocumentApprentice().getDocument());
            row.createCell(4).setCellValue(lateArrivalList.get(i).getDocumentApprentice().getFullName());
        }
        FileOutputStream outputStream = new FileOutputStream(temporalfile);
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();
        System.out.println("Archivo courses excel creado");
    }   

    @Override
    public void sendEmailAttachment(String to, String subject, long document) throws Exception {
        Properties props = new Properties();
        props.put("mail.transport.protocol", Constants.PROTOCOL);
        props.put("mail.smtp.host", Constants.HOST);
        props.put("mail.smtp.socketFactory.port", Constants.SOCKET_FACTORY_PORT);
        props.put("mail.smtp.socketFactory.class", Constants.SOCKET_FACTORY);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", Constants.PORT);
        props.put("mail.smtp.user", Constants.USER);
        props.put("mail.smtp.password", Constants.PASSWORD);
        props.put("mail.smtp.starttls.enable", "true");

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);
        String bodyMessage = setMessage();
        
        exporExcelTemp(document);
        
        MimeBodyPart text = new MimeBodyPart();
        text.setContent(bodyMessage, "text/html; charset=utf-8");//cuerpo del mensaje
        MimeBodyPart attach = new MimeBodyPart();
        DataHandler dh = new DataHandler(new FileDataSource(temporalfile));
        attach.setDataHandler(dh);
        attach.setFileName(dh.getName());
        
        MimeMultipart mp = new MimeMultipart();
        mp.addBodyPart(text);
        mp.addBodyPart(attach);
        mp.setSubType("mixed");
        
         
        
        
        
        InternetAddress from = new InternetAddress(props.getProperty("mail.smtp.user"));
        message.setFrom(from);
        message.setSubject(subject);
        message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(to));//destinatario
        message.setContent(mp);//cuerpo del mensaje

        Transport transport = session.getTransport(Constants.PROTOCOL);
        transport.connect(props.getProperty("mail.smtp.host"), props.getProperty("mail.smtp.user"),
                props.getProperty("mail.smtp.password"));
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();
        temporalfile.deleteOnExit();

    }
    
    
   
}
