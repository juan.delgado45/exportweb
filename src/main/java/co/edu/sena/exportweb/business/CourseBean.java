/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package co.edu.sena.exportweb.business;

import co.edu.sena.exportweb.model.Course;
import co.edu.sena.exportweb.persistence.ICourseDAO;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class CourseBean implements CourseBeanLocal {

    @EJB
    private ICourseDAO courseDAO;

    @Override
    public Course findById(Integer id) throws Exception {
        if(id == 0)
        {
            throw new Exception("El id es obligatorio");
        }
        
        return courseDAO.findById(id);
    }

    @Override
    public List<Course> findAll() throws Exception {
        return courseDAO.findAll();
    }

    @Override
    public void exportExcel(OutputStream outputStream) throws Exception {
        List<Course> courses= findAll();
        Workbook workbook= new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Fichas");
        //estilo para encabezados
        CellStyle styleHeader = workbook.createCellStyle();
        styleHeader.setFillForegroundColor(IndexedColors.BLUE_GREY.getIndex());//color letra
        styleHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleHeader.setBorderBottom(BorderStyle.THIN);
        styleHeader.setBorderBottom(BorderStyle.THIN);
        styleHeader.setBorderBottom(BorderStyle.THIN);
        styleHeader.setBorderBottom(BorderStyle.THIN);
        
        String[] titles = {"id","programa"};
        Row row = sheet.createRow(0);
        //crear el encabezado
        for (int i = 0; i < titles.length; i++) {
            Cell cell = row.createCell(i);
            cell.setCellStyle(styleHeader);
            cell.setCellValue(titles[i]);
        }
        
        //colocamos los datos en las siguientes filas
        for (int i = 0; i < courses.size(); i++) {
            row = sheet.createRow(i+1);
            row.createCell(0).setCellValue(courses.get(i).getId());
            row.createCell(1).setCellValue(courses.get(i).getCareer());
        }
        
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();
        System.out.println("Archivo courses excel creado");
    }   
    
}
