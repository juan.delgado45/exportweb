/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/SessionLocal.java to edit this template
 */
package co.edu.sena.exportweb.business;

import co.edu.sena.exportweb.model.ApprenticeCount;
import co.edu.sena.exportweb.model.LateArrival;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface LateArrivalBeanLocal {

    public LateArrival findById(Integer id) throws Exception;

    public List<LateArrival> findAll() throws Exception;

    public List<LateArrival> findByDateRange(Date date1, Date date2) throws Exception;

    public List<ApprenticeCount> findGroupByApprentice() throws Exception;

    public void exporExcel(OutputStream outputStream, Date date1, Date date2) throws Exception;

   public void exportPDFLateArrivals(OutputStream outputStream) throws Exception;
}
