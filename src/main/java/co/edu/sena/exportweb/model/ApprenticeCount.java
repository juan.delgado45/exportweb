/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.exportweb.model;

/**
 *
 * @author Aprendiz
 */
public class ApprenticeCount {
    private Long document;
    private Long count;
    
    public ApprenticeCount(Long document, Long count ) {
        this.document = document;
        this.count = count;
    }

    public Long getDocument() {
        return document;
    }

    public void setDocument(Long document) {
        this.document = document;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
    
    

}
