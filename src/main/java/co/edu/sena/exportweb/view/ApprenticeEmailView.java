/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package co.edu.sena.exportweb.view;

import co.edu.sena.exportweb.business.ApprenticeBeanLocal;
import co.edu.sena.exportweb.utils.MessageUtils;
import javax.ejb.EJB;
import org.primefaces.component.inputtext.InputText;

/**
 *
 * @author Aprendiz
 */
public class ApprenticeEmailView {
    
    private InputText txtEmail;
    private InputText txtSubject;
    private InputText txtDocumet;

    @EJB
    private ApprenticeBeanLocal apprenticeBean;
    
    public InputText getTxtEmail() {
        return txtEmail;
    }

    public void setTxtEmail(InputText txtEmail) {
        this.txtEmail = txtEmail;
    }

    public InputText getTxtSubject() {
        return txtSubject;
    }

    public void setTxtSubject(InputText txtSubject) {
        this.txtSubject = txtSubject;
    }

    public InputText getTxtDocumet() {
        return txtDocumet;
    }

    public void setTxtDocumet(InputText txtDocumet) {
        this.txtDocumet = txtDocumet;
    }
    
    

    /**
     * Creates a new instance of ApprenticeEmailView
     */
    public ApprenticeEmailView() {
    }
    
    public void sendEmail(){
        try {
            apprenticeBean.sendEmail(txtEmail.getValue().toString(), txtSubject.getValue().toString());
            MessageUtils.addInfoMessage("Mensaje enviado al correo electronico");
        } catch (Exception e) {
            MessageUtils.addErrorMessage(e.getMessage());
        }
}
    public void sendEmailAttachment()
    {
         try {
            apprenticeBean.sendEmailAttachment(txtEmail.getValue().toString(), txtSubject.getValue().toString(),
                    Long.parseLong(txtDocumet.getValue().toString()));
            MessageUtils.addInfoMessage("Mensaje enviado al correo electronico");
        } catch (Exception e) {
            MessageUtils.addErrorMessage(e.getMessage());
        }
    }
}
