/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package co.edu.sena.exportweb.business;

import co.edu.sena.exportweb.model.ApprenticeCount;
import co.edu.sena.exportweb.model.Course;
import co.edu.sena.exportweb.model.LateArrival;
import co.edu.sena.exportweb.persistence.ILateArrivalDAO;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.properties.TextAlignment;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class LateArrivalBean implements LateArrivalBeanLocal {

    @EJB
    private ILateArrivalDAO lateArrivalDAO;

    @Override
    public LateArrival findById(Integer id) throws Exception {
        if (id == 0) {
            throw new Exception("El id es obligatorio");
        }

        return lateArrivalDAO.findById(id);
    }

    @Override
    public List<LateArrival> findAll() throws Exception {
        return lateArrivalDAO.findAll();
    }

    @Override
    public List<LateArrival> findByDateRange(Date date1, Date date2) throws Exception {
        return lateArrivalDAO.findByDateRange(date1, date2);
    }

    @Override
    public List<ApprenticeCount> findGroupByApprentice() throws Exception {
        return lateArrivalDAO.findGroupByApprentice();
    }

    @Override
    public void exporExcel(OutputStream outputStream, Date date1, Date date2) throws Exception {
        List<LateArrival> arrivals = findByDateRange(date1, date2);
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Llegadas tarde");
        //estilo para encabezados
        CellStyle styleHeader = workbook.createCellStyle();
        styleHeader.setFillForegroundColor(IndexedColors.RED.getIndex());//color letra
        styleHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleHeader.setBorderBottom(BorderStyle.THIN);
        styleHeader.setBorderBottom(BorderStyle.THIN);
        styleHeader.setBorderBottom(BorderStyle.THIN);
        styleHeader.setBorderBottom(BorderStyle.THIN);

        String[] titles = {"Id", "Fecha", "Observaciones", "Documento aprendiz", "Nombre aprendiz"};
        Row row = sheet.createRow(0);
        //crear el encabezado
        for (int i = 0; i < titles.length; i++) {
            Cell cell = row.createCell(i);
            cell.setCellStyle(styleHeader);
            cell.setCellValue(titles[i]);
        }
        //estilo para celdas con fecha
        CreationHelper createnHelper = workbook.getCreationHelper();
        CellStyle dataCellStyle = workbook.createCellStyle();
        dataCellStyle.setDataFormat(createnHelper.createDataFormat().getFormat("yyyy-MM-dd"));

        //colocamos los datos en las siguientes filas
        for (int i = 0; i < arrivals.size(); i++) {
            row = sheet.createRow(i + 1);
            row.createCell(0).setCellValue(arrivals.get(i).getId());
            Cell dateArraivalcell = row.createCell(1);
            dateArraivalcell.setCellStyle(dataCellStyle);
            dateArraivalcell.setCellValue(arrivals.get(i).getDateArrival());
            row.createCell(2).setCellValue(arrivals.get(i).getObservations());
            row.createCell(3).setCellValue(arrivals.get(i).getDocumentApprentice().getDocument());
            row.createCell(4).setCellValue(arrivals.get(i).getDocumentApprentice().getFullName());

        }

        workbook.write(outputStream);
        workbook.close();
        outputStream.close();
        System.out.println("Archivo late arrivals  excel creado");
    }
    
    @Override   
public void exportPDFLateArrivals(OutputStream outputStream) throws Exception {
        PdfWriter pdfWriter = new PdfWriter(outputStream);
        PdfDocument pdfDocument = new PdfDocument(pdfWriter);
        Document document = new Document(pdfDocument);
        
        Paragraph paragraph = new Paragraph("Reporte de cantidad de llegdas tarde por aprendiz");
        paragraph.setFontSize(14);
        paragraph.setTextAlignment(TextAlignment.CENTER);
        paragraph.setBold();
        paragraph.setFontColor(ColorConstants.RED);
        document.add(paragraph);
        document.add(new Paragraph("")); //salto de linea
        
        float[] columnsWidths = {150F, 150F};
        Table table = new Table(columnsWidths) {};
        
        table.addCell(new com.itextpdf.layout.element.Cell().add(new Paragraph("Documento Aprendiz")));
        table.addCell(new com.itextpdf.layout.element.Cell().add(new Paragraph("Cnatidad llegadas tarde")));
        
         List<ApprenticeCount> counts = findGroupByApprentice();
         for (ApprenticeCount apprenticeCount : counts) {
             
             table.addCell(new com.itextpdf.layout.element.Cell().add(new Paragraph(String.valueOf(apprenticeCount.getDocument()))));
             table.addCell(new com.itextpdf.layout.element.Cell().add(new Paragraph(String.valueOf(apprenticeCount.getCount())))); 
        }
         document.add(table);
         document.close();
         pdfWriter.close();
         outputStream.close();
         System.out.println("Pdf creado!");
    }
   
}
